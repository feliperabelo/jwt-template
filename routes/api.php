<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(['middleware' => ['api']], function () {
    Route::post('v1/user-login', 'User\LoginController@login');
    Route::post('v1/user-logout', 'User\LoginController@logout');
    Route::get('v1/clientes', 'ClienteController@index');
});

Route::group(['middleware' => ['api', 'manage_token:api_user,ROLE_USER_ADMIN|ROLE_USER_SALES']], function () {
    Route::post('v1/user/home', 'User\Profile\HomeController@home');
}); 