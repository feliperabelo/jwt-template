<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'name' => 'Felipe',
            'email' => 'email@gmail.com',
            'password' => bcrypt('senha'),
            'role' => 'ROLE_USER_ADMIN',
        ]);        
    }
}
